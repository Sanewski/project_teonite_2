import React, { Component } from "react";
import ReactDOM from "react-dom";
import makeAnimated from "react-select/animated";
import MySelect from "./MySelect.js";

const animatedComponents = makeAnimated();


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionSelected: null,
      options: [],
      results: [],
    };
  }

  componentDidMount() {
    fetch('/authors/')
      .then(response => response.json())
      .then(data => {
           let options=[]
           for (var x in data) {
             options.push({"value": x, "label": data[x]})
            }


        this.setState({...this.state,options:options});
      });
}
getResults(selected) {
  const results = []
  console.log(selected)
  if (selected[0] === undefined){
    this.setState({...this.state,results:[], optionSelected:[]});
  }
  
  for (var author of selected){
    fetch('/stats/' + author["value"])
      .then(response => response.json())
      .then(data => {
          for (var x in data) {
            results.push({"word": x, "count": data[x]})
            }

        this.setState({...this.state,results: results});
      });
    }
  
}

  handleChange = selected => {
    this.setState({
      optionSelected: selected
    });
    this.getResults(selected)
  };

  render() {
    return (
        <div>
        <h2>Authors:</h2>
        <MySelect
            options={this.state.options}
            isMulti
            closeMenuOnSelect={true}
            hideSelectedOptions={true}
            components={animatedComponents}
            onChange={this.handleChange}
            allowSelectAll={true}
            value={this.state.optionSelected}
        />
        <p>
            results: {JSON.stringify(this.state.results)}
        </p>
        <p>
            option selected: {JSON.stringify(this.state.optionSelected)}
        </p>
        
        <table>
          <tr>
            <td>
              Nr
            </td>
            <td>
              Word
            </td>
            <td>
              Count
            </td>
          </tr>
          {this.state.results.map((item =>
          <tr>
          <td>
            {this.state.results.indexOf(item) + 1}
          </td> 
          <td>
            {item.word}
          </td>
          <td>
            {item.count}
          </td>
          
          </tr>
          ))}
        </table>
      </div>
    );
  }
}


ReactDOM.render(<App />, document.getElementById("root"));